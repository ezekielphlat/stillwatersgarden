const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    categoryId: { type: mongoose.Schema.Types.ObjectId, ref: "Category" },
    typeId: { type: mongoose.Schema.Types.ObjectId, ref: "Type" },
    imageUrl: { type: String },
    content: { type: String, required: true },
    active: { type: Boolean, required: true, default: true }
  },
  { timestamps: true } // adds createdAt and modifiedAt to the database
);
module.exports = mongoose.model("Content", schema);
