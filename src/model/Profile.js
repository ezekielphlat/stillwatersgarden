const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    userId: { type: mongoose.Schema.Types.ObjectId, ref: "User", unique: true },
    firstName: { type: String, lowercase: true },
    lastName: { type: String, lowercase: true },
    profilePicture: { type: String },
    roleId: { type: mongoose.Schema.Types.ObjectId, ref: "Role" },
    genderId: { type: mongoose.Schema.Types.ObjectId, ref: "Gender" }
  },
  { timestamps: true, toObject: { virtuals: true }, toJSON: { virtuals: true } } // adds createdAt and modifiedAt to the database
);

/* eslint-disable */
schema.virtual("fullName").get(function() {
  return this.firstName + " " + this.lastName;
});
/*eslint-disable */

module.exports = mongoose.model("Profile", schema);
