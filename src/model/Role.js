const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, lowercase: true, unique: true },
    description: { type: String, lowercase: true },
    active: { type: Boolean, required: true, default: true }
  },
  { timestamps: true } // adds createdAt and modifiedAt to the database
);

module.exports = function getInitalRoles() {
  this.find({}).then(console.log("working"));
};

module.exports = mongoose.model("Role", schema);
