const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const uniqueValidator = require("mongoose-unique-validator");

const schema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    email: {
      type: String,
      required: true,
      lowercase: true,
      index: true,
      unique: true
    },
    role: { type: mongoose.Schema.Types.ObjectId, ref: "Role" },
    passwordHash: { type: String, required: true },
    active: { type: Boolean, required: true, default: true },
    confirmed: { type: Boolean, saveIntialRolesrequired: true, default: false },
    confirmationToken: { type: String, default: "" }
  },
  { timestamps: true } // adds createdAt and modifiedAt to the database
);

/* eslint-disable */
schema.virtual("fullName").get(function() {
  return this.firstName + " " + this.lastName;
});
/*eslint-disable */

schema.methods.setPassword = function setPassword(password) {
  this.passwordHash = bcrypt.hashSync(password, 10);
};
schema.methods.setConfirmationToken = function setPassword() {
  this.confirmationToken = this.generateJWT();
};

schema.methods.generateConfirmationUrl = function generateConfirmationUrl() {
  return `${process.env.HOST}/confirmation/${this.confirmationToken}`;
};

schema.methods.isValidPassword = function isValidPassword(password) {
  return bcrypt.compareSync(password, this.passwordHash);
};

schema.methods.generateJWT = function generateJWT() {
  return jwt.sign(
    {
      email: this.email,
      confirm: this.confirm,
      role: this.role.name
    },
    process.env.JWT_SECRET
  );
};

schema.methods.toAuthJSON = function toAuthJSON() {
  return {
    email: this.email,
    role: this.role.name,
    confirmed: this.confirm,
    token: this.generateJWT()
  };
};

schema.plugin(uniqueValidator, { message: "This email is already taken" });

module.exports = mongoose.model("User", schema);
