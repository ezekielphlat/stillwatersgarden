const mongoose = require("mongoose");

const schema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    count: { type: Boolean, required: true, default: false }
  },
  { timestamps: true } // adds createdAt and modifiedAt to the database
);

module.exports = mongoose.model("AppRunCount", schema);
