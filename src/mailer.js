const nodemailer = require("nodemailer");

const from = '"Still Waters Garden" <info@stillwatergarden.com>';
function setup() {
  return nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS
    }
  });
}

module.exports = function sendConfirmationEmail(user) {
  const transport = setup();
  const email = {
    from,
    to: user.email,
    subject: "welcome to Still Waters Garden",
    text: `
    welcome to Still Waters Garden please confirm your email.
        
    Please activate your account by clicking on the link bellow. 
    ${user.generateConfirmationUrl()}
        `
  };
  transport.sendMail(email);
};
module.exports = function sendResetPasswordEmail(user) {
  const transport = setup();
  const email = {
    from,
    to: user.email,
    subject: "Reset password",
    text: `
        To reset password follow this link
        
        ${user.generateResetPasswordLink()}
        `
  };
  transport.sendMail(email);
};

module.exports = function test() {};
