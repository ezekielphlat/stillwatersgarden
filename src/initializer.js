const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const AppRunCount = require("./model/AppRunCount");
const Role = require("./model/Role");
const Gender = require("./model/Gender");
const User = require("./model/User");
const Category = require("./model/Category");
const Type = require("./model/Type");

module.exports = function initializer() {
  // initialize categories
  const home = new Category({
    _id: new mongoose.Types.ObjectId(),
    name: "Home",
    description: "Home page content"
  });
  const about = new Category({
    _id: new mongoose.Types.ObjectId(),
    name: "About",
    description: "About page content"
  });
  const buyingRenting = new Category({
    _id: new mongoose.Types.ObjectId(),
    name: "BuyingRenting",
    description: "Buying and Renting page content"
  });
  const renovationConstruction = new Category({
    _id: new mongoose.Types.ObjectId(),
    name: "RenovationConstruction",
    description: "Renovation and Construction page content"
  });

  // Initialize Types of home category
  const slider = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "slider",
    description: "slides on the home page size 700x700px",
    categoryId: home._id
  });
  const facilitiesCard = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "facilities_cards",
    description: "Facilities section of the home page size 208x138px",
    categoryId: home._id
  });
  const review = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "review",
    description:
      "Review section of the home page title is always going to be resident",
    categoryId: home._id
  });
  // Initialize types of About category
  const aboutUs = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "about_us",
    description:
      "About section of the about page that also contain the estate image",
    categoryId: about._id
  });
  // Initialize types of buying and renting
  const steps = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "buying_process",
    description:
      "Steps to buying section of the buying page. title would be steps 1 and so on",
    categoryId: buyingRenting._id
  });
  const otherFacilities = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "other_facilities",
    description: "Other Facilities images in the buying and renting section",
    categoryId: buyingRenting._id
  });
  // Initialize types of renovaiton and construction
  const renovation = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "renovation",
    description: "renovation section of the renovation and construction page",
    categoryId: renovationConstruction._id
  });
  const construction = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "construction",
    description: "construction section of the renovation and construction page",
    categoryId: renovationConstruction._id
  });
  const constructionImages = new Type({
    _id: new mongoose.Types.ObjectId(),
    name: "construction_images",
    description:
      "construction Images section of the renovation and construction page",
    categoryId: renovationConstruction._id
  });

  // Initialize gender
  const male = new Gender({
    _id: new mongoose.Types.ObjectId(),
    name: "male"
  });
  const female = new Gender({
    _id: new mongoose.Types.ObjectId(),
    name: "female"
  });

  // Initialize roles
  const admin = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "admin",
    description: "admin role"
  });
  const resident = new Role({
    _id: new mongoose.Types.ObjectId(),
    name: "resident",
    description: "resident role"
  });

  // Initialize users
  const testAdmin = new User({
    _id: new mongoose.Types.ObjectId(),
    email: "testadmin@test.com",
    passwordHash: bcrypt.hashSync("test", 10),
    role: admin._id,
    active: true,
    confirmed: true
  });
  const testResident = new User({
    _id: new mongoose.Types.ObjectId(),
    email: "testresident@test.com",
    passwordHash: bcrypt.hashSync("test", 10),
    role: resident._id,
    active: true,
    confirmed: true
  });
  const fistTimeRun = new AppRunCount({
    _id: new mongoose.Types.ObjectId(),
    count: true
  });

  AppRunCount.find({}).then(count => {
    if (Object.keys(count).length === 0) {
      admin.save();
      resident.save();
      male.save();
      female.save();
      testAdmin.save();
      testResident.save();
      // Save Categories
      home.save();
      about.save();
      buyingRenting.save();
      renovationConstruction.save();

      // Save types
      slider.save();
      facilitiesCard.save();
      review.save();
      aboutUs.save();
      steps.save();
      otherFacilities.save();
      renovation.save();
      construction.save();
      constructionImages.save();
      fistTimeRun.save();
      console.log("initial data is saved");
    } else {
      console.log("This app has ran before");
    }
  });
};
