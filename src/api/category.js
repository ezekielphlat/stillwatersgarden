const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");
const Category = require("../model/Category");

const upload = multer();
const router = express.Router();

router.get("/", (req, res) => {
  Category.find({}).then(cats => {
    if (cats) {
      res.status(200).json({ cats });
      // console.log(cats);
    } else {
      res.status(400).json({ errors: { global: "invalid credentials" } });
    }
  });
});

router.post("/save", upload.none(), (req, res) => {
  const { name, description } = req.body;
  // console.log(req.body);

  const category = new Category({
    _id: mongoose.Types.ObjectId(),
    name,
    description
  });

  category
    .save()
    .then(cat => {
      res.status(200).json({ category: cat });
    })
    .catch(err => res.status(200).json({ errors: { global: err } }));
});
module.exports = router;
