const express = require("express");
const multer = require("multer");
const jwt = require("jsonwebtoken");
const User = require("../model/User");
const { sendResetPasswordEmail } = require("../mailer");

const upload = multer();
const router = express.Router();

// Login
router.post("/", upload.none(), (req, res) => {
  const credentials = req.body;
  // console.log(credentials);

  User.findOne({ email: credentials.email })
    .populate("role")
    .then(user => {
      // console.log(user.role.name);
      if (
        user &&
        user.isValidPassword(credentials.password) &&
        user.active === true &&
        user.confirmed === true
      ) {
        res.json({ user: user.toAuthJSON() });
      } else {
        res.status(400).json({ errors: { global: "invalid credentials" } });
      }
    });
});

// Confirm token
router.post("/confirmation/:token", (req, res) => {
  const token = req.params.token;
  console.log(token);
  User.findOneAndUpdate(
    { confirmationToken: token },
    { confirmationToken: " ", confirmed: true },
    { new: true }
  ).then(user =>
    user ? res.json({ user: user.toAuthJSON() }) : res.status(400).json({})
  );
});

// request password reset
router.post("/reset_password_request", (req, res) => {
  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      sendResetPasswordEmail(user);
      res.json({});
    } else {
      res.status(400).json({ errors: { global: "There is no such email" } });
    }
  });
});

// validate token
router.post("/validate_token", (req, res) => {
  jwt.verify(req.body.token, process.env.JWT_SECRET, err => {
    if (err) {
      res.status(401).res.json({});
    } else {
      res.json({});
    }
  });
});

// reset password
router.post("/reset_password", (req, res) => {
  const { password, token } = req.body.data;
  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      res.status(401).json({ errors: { global: "Invalid token" } });
    } else {
      User.findOne({ _id: decoded._id }).then(user => {
        if (user) {
          user.setPassword(password);
          user.save().then(() => res.json({}));
        } else {
          res.status(404).json({ errors: { global: "Invalid token" } });
        }
      });
    }
  });
});

module.exports = router;
