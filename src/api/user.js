// TODO Create routes snippet
const express = require("express");
const multer = require("multer");
const mongoose = require("mongoose");

const User = require("../model/User");
const Profile = require("../model/Profile");
const parseErrors = require("../utils/parseErrors");
const { sendConfirmationEmail } = require("../mailer");
const storage = require("../utils/multerStorage");

// TODO: encapsulate the storage function

const upload = multer({ storage });
const router = express.Router();

// activate user account
router.put("/activate/:id", (req, res) => {
  const { id } = req.params;
  // console.log(id);
  User.findOneAndUpdate({ _id: id }, { active: true }, { new: true }).then(
    user => user && res.json({ user })
  );
});

// suspend user account
router.put("/suspend/:id", (req, res) => {
  const { id } = req.params;
  // console.log(id);

  User.findOneAndUpdate({ _id: id }, { active: false }, { new: true }).then(
    user => user && res.json({ user })
  );
});

router.get("/", (req, res) => {
  Profile.find({})
    .populate("userId")
    .populate("roleId")
    .populate("genderId")
    .then(users => {
      if (users) {
        res.status(200).json({ users });
        // console.log(users);
      } else {
        res.status(400).json({
          errors: { global: "invalid credentials, unable to get list of users" }
        });
      }
    });
});
router.post("/", upload.single("file"), (req, res) => {
  // console.log(req.body);
  // console.log("file: ", req.file);
  const filePath = req.file.path;
  const { email, password, role, firstName, lastName, gender } = req.body;

  // Create new user instance
  const user = new User({
    _id: mongoose.Types.ObjectId(),
    email,
    confirmed: false,
    role
  });
  // hash the user password
  user.setPassword(password);

  // create new profile instance
  const profile = new Profile({
    _id: mongoose.Types.ObjectId(),
    userId: user._id,
    firstName,
    lastName,
    profilePicture: filePath,
    roleId: role,
    genderId: gender
  });

  user.setConfirmationToken();
  // Save the instance of the user
  user
    .save()
    .then(userRecord => {
      sendConfirmationEmail(userRecord);
      res.status(200).json({ user: userRecord.toAuthJSON() });
      // Save the instance ot the user profile
      profile
        .save()
        .then(p => {
          console.log(p._id);
        })
        .catch(err => console.log(err));
    })
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
});

module.exports = router;
