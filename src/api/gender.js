const express = require("express");
const Gender = require("../model/Gender");

// import mongoose =  require ( "mongoose";

const router = express.Router();

router.get("/", (req, res) => {
  Gender.find({}).then(genders => {
    if (genders) {
      res.status(200).json({ genders });
      // console.log(genders);
    } else {
      res.status(400).json({ errors: { global: "invalid credentials" } });
    }
  });
});

module.exports = router;
