const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");

const Type = require("../model/Type");

const upload = multer();
const router = express.Router();
router.get("/", (req, res) => {
  Type.find({})
    .then(typesData => {
      if (typesData) {
        res.status(200).json({ types: typesData });
      }
    })
    .catch(err => {
      res.status(400).json({ errors: { global: err } });
    });
});

router.get("/:categoryId", (req, res) => {
  Type.find({ categoryId: req.params.categoryId })
    .then(typesData => {
      if (typesData) {
        res.status(200).json({ types: typesData });
      }
    })
    .catch(err => {
      res.status(400).json({ errors: { global: err } });
    });
});

router.post("/save", upload.none(), (req, res) => {
  const { categoryId, name, description } = req.body;
  const type = new Type({
    _id: mongoose.Types.ObjectId(),
    categoryId,
    name,
    description
  });
  type
    .save()
    .then(newType => {
      if (type) {
        res.status(200).json({ type: newType });
      }
    })
    .catch(err => {
      res.status(400).json({ errors: { global: err } });
    });
});
module.exports = router;
