const express = require("express");
const Role = require("../model/Role");

// import mongoose =  require ( "mongoose";

const router = express.Router();

router.get("/", (req, res) => {
  Role.find({}).then(roles => {
    if (roles) {
      res.status(200).json({ roles });
      // console.log(roles);
    } else {
      res.status(400).json({ errors: { global: "invalid credentials" } });
    }
  });
});

module.exports = router;
