const express = require("express");
const mongoose = require("mongoose");
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const storage = require("../utils/multerStorage");
const parseErrors = require("../utils/parseErrors");

const Content = require("../model/Content");
const Type = require("../model/Type");

const upload = multer({ storage });
const router = express.Router();

router.get("/", (req, res) => {
  Content.find({})
    .populate("categoryId")
    .populate("typeId")
    .then(contentData => {
      if (contentData) {
        res.status(200).json({ content: contentData });
      }
    })
    .catch(err => res.status(400).json({ errors: { global: err } }));
});

// delete content
router.put("/remove/:id", (req, res) => {
  const { id } = req.params;
  Content.findOneAndDelete({ _id: id }).then(contentData => {
    if (contentData.imageUrl !== "") {
      fs.unlink(path.join("public/", contentData.imageUrl), err => {
        if (err) throw err;
        console.log("successfully deleted");
      });
    }
    res.json({ contentData });
  });
});

router.get("/:type", (req, res) => {
  const type = req.params.type;

  Type.findOne({ name: type }).then(typeData => {
    if (Object.keys(typeData).length !== 0) {
      Content.find({ typeId: typeData._id }).then(contentData => {
        res.status(200).json({ content: contentData });
      });
    }
  });
});

router.post("/save", upload.single("file"), (req, res) => {
  let filePath = "";

  if (req.file) {
    filePath = req.file.path.slice(6);
  }
  const { title, categoryId, typeId, content } = req.body;
  const newContent = new Content({
    _id: mongoose.Types.ObjectId(),
    title,
    categoryId,
    typeId,
    imageUrl: filePath,
    content
  });

  newContent
    .save()
    .then(contentData => {
      res.status(200).json({ content: contentData._id });
    })
    .catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
});
module.exports = router;
