const compression = require("compression");
const express = require("express");
const dotenv = require("dotenv");
const initializer = require("./initializer");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const homeController = require("./controller/homeController");
const aboutController = require("./controller/aboutController");
const buyingController = require("./controller/buyingController");
const renovationController = require("./controller/renovationController");
const dashboardController = require("./controller/dashboardController");
const informationController = require("./controller/informationController");
const userController = require("./controller/userController");
const confirmationController = require("./controller/confirmationController");

const auth = require("./api/auth");
const role = require("./api/role");
const gender = require("./api/gender");
const user = require("./api/user");
const category = require("./api/category");
const type = require("./api/type");
const content = require("./api/content");

dotenv.config();
const app = express();
app.use(compression());

mongoose.connect(process.env.MONGODB_URL.toString(), {
  useNewUrlParser: true,
  useCreateIndex: true
});
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

const PORT = process.env.PORT || 30200;

// initialize roles and default user data
initializer();

// set up template engine
app.set("view engine", "ejs");

// map static files
app.use(express.static("./public"));

// Controllers route
app.use("/", homeController);
app.use("/about", aboutController);
app.use("/buying_renting", buyingController);
app.use("/confirmation", confirmationController);
app.use("/renovation_construction", renovationController);
app.use("/dashboard", dashboardController);
app.use("/information", informationController);
app.use("/user", userController);

// Api routes
app.use("/api/auth", auth);
app.use("/api/role", role);
app.use("/api/gender", gender);
app.use("/api/user", user);
app.use("/api/category", category);
app.use("/api/type", type);
app.use("/api/content", content);

app.listen(PORT, () => console.log("app running on port ", PORT));
