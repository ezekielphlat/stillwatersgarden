const multer = require("multer");

const fs = require("fs");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const d = new Date();
    const imageDir = `public/assets/images/uploads/${d.getDate()}-${d.getMonth() +
      1}-${d.getFullYear()}/`;
    if (!fs.existsSync(imageDir)) {
      fs.mkdirSync(imageDir);
    }
    const fileDir = `uploads/${d.getDate()}-${d.getMonth() +
      1}-${d.getFullYear()}/`;
    if (!fs.existsSync(fileDir)) {
      fs.mkdirSync(fileDir);
    }

    if (file.mimetype.match(/image.*/)) {
      cb(null, imageDir.toString());
    } else {
      cb(null, fileDir.toString());
    }
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}-${file.originalname.replace(/\s/gi, "")}`);
  }
});

module.exports = storage;
