const express = require("express");

const router = express.Router();
const pageData = {
  active: "user",
  type: "backend",
  genders: [],
  roles: []
};

router.get("/", (req, res) => {
  res.render("admin/user", { data: pageData });
});

module.exports = router;
