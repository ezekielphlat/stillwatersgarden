const express = require("express");

const router = express.Router();
// const pageData = {
//   active: "information",
//   type: "backend"
// };
router.get("/:token", (req, res) => {
  res.render("email_confirmation", { token: req.params.token });
});

module.exports = router;
