const express = require("express");

const router = express.Router();

router.get("/", (req, res) => {
  res.render("renovation_construction");
});

module.exports = router;
