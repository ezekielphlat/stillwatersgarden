const express = require("express");

const router = express.Router();
const pageData = {
  active: "dashboard",
  type: "backend"
};
router.get("/", (req, res) => {
  res.render("admin/dashboard", { data: pageData });
});

module.exports = router;
