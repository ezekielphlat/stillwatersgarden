const express = require("express");

const router = express.Router();
const pageData = {
  active: "information",
  type: "backend"
};
router.get("/", (req, res) => {
  res.render("admin/information", { data: pageData });
});

module.exports = router;
