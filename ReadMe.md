## About

---

Still waters Garden project is an estate information management system for an estate located around lekki Lagos Nigeria. It allows resident of the estate to login and view basic information about the estate administrative activity instead of looking for someone to ask.
The system would also allow an administrator to login and upload informations, files and document and to control user activity on the site.

## Task list

---

- [x] Create Static layout for client
- [ ] Comet Haley
- [x] Create static layout for admin
- [x] Make Admin and Client view dynamic using ejs
- [x] Create Login Page and Login function
- [x] Create User and Admin route after authentication
- [x] Create Registration Form and package the form Data
- [x] Create registration api route without sending confirmation mail
- [x] Send confirmation email to client
- [ ] Create confirmation page and api for user
- [ ] Create reset password page and api for user
- [ ] Create Upload form for Category, Type and information
- [ ] Create information view for at client side
- [ ] Create Edit Delete and Status change function for information and registered user.
- [ ] Create User Profile view, update and settings on client side.
