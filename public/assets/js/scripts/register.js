/*eslint-disable */
$(document).ready(function() {
  var $email = $("#txtEmail");
  var $fname = $("#txtFname");
  var $lname = $("#txtLname");
  var $allInput = $("input, textarea");
  var $message = $("#messageGlobal");
  var $ddlRole = $("#ddlRole");
  var $picture = $("#profilePicture");
  var $roleTemplate = $("#roleTemplate");
  var $genderTemplate = $("#genderTemplate");
  var $userTableTemplate = $("#userTableTemplete");
  var $divGenders = $("#divGenders");
  var $userTableBody = $("#userTableBody");

  //user table
  //Get user funciton and pupulate user table
  function populateUserTable() {
    getUsers().then(users => {
      let newUsers;
      if (users) {
        newUsers = addIndexTo(users);
      }

      var template = Mustache.render($userTableTemplate.html(), {
        users: newUsers
      });
      console.log(newUsers);
      $userTableBody.html(template);
    });
  }
  populateUserTable();

  $(document).on("click", ".btnSuspend", function() {
    var tr = $(this).closest("tr");

    axios.put("/api/user/suspend/" + tr.data("id")).then(user => {
      if (user) {
        populateUserTable();
        $(this).addClass("hidden");
        tr.find(".btnActivate").removeClass("hidden");
      }
    });
  });
  $(document).on("click", ".btnActivate", function() {
    var tr = $(this).closest("tr");
    console.log(tr.data("id"));
    axios.put("/api/user/activate/" + tr.data("id")).then(user => {
      if (user) {
        populateUserTable();
        $(this).addClass("hidden");
        tr.find(".btnActivate").removeClass("hidden");
      }
    });

    $(this).addClass("hidden");
    tr.find(".btnSuspend").removeClass("hidden");
  });

  // getRoles().then(function(r) {
  //   const { roles } = r.data;
  //   var html = ejs.render("", { roles: roles });
  //   console.log(roles);
  // });
  $.validator.setDefaults({
    errorClass: "help-block",
    highlight: function(element) {
      $(element)
        .closest(".form-group")
        .addClass("has-error");
    },
    unhighlight: function(element) {
      $(element)
        .closest(".form-group")
        .removeClass("has-error");
    }
  });

  $("#regForm").validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      confirmEmail: {
        required: true,
        equalTo: "#txtEmail"
      },
      fname: {
        required: true
      },
      lname: {
        required: true
      }
    },
    submitHandler: function(form) {
      RegisterUser();
      return false;
    }
  });

  getRoles()
    .then(roles => {
      // const { roles } = r.data;
      if (roles) {
        // console.log(roles);

        var template = Mustache.render($roleTemplate.html(), {
          roles: roles
        });
        $ddlRole.html(template);
      }
    })
    .catch(errors => {
      console.log(errors);
    });

  getGenders().then(genders => {
    if (genders) {
      var newGender = genders.map(
        g => (g = { index: genders.indexOf(g), ...g })
      );
      var template = Mustache.render($genderTemplate.html(), {
        genders: newGender
      });
      $divGenders.html(template);
    }
  });

  function RegisterUser() {
    $message.addClass("hidden");
    var regForm = new FormData();
    regForm.set("email", $email.val());
    regForm.set("password", 12345);
    regForm.set("role", $ddlRole.val());
    regForm.set("firstName", $fname.val());
    regForm.set("lastName", $lname.val());

    regForm.set("file", $("input[type=file]")[0].files[0]);
    regForm.set("gender", $("input[name=gender]:checked").val());
    console.log();
    axios({
      method: "post",
      url: "/api/user",
      data: regForm
    })
      .then(function(userData) {
        const { user } = userData.data;
        //clear all input.
        $allInput.val("");
        $message.fadeIn(3000);
        $message.removeClass("hidden alert-danger");
        $message.addClass("alert-success");
        $message.html("You have successfully registered " + user.email);
      })
      .catch(function(err) {
        console.log(err.errors);
        if (err) {
          $message.fadeIn(3000);

          $message.removeClass("hidden alert-success");
          $message.addClass("alert-danger");
          $message.html(
            "Something went wrong please try the registration process again."
          );
        }
      });
  }
});
