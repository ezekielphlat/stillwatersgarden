/* eslint-disable */
$(document).ready(function() {
  //forms
  var $newCategoryForm = $("#newCategoryForm");
  var $newTypeForm = $("#newTypeForm");

  // form field
  //category
  var $catName = $("#catName");
  var $catDescription = $("#catDescription");
  var $categoryTableTemplate = $("#categoryTableTemplate");
  var $categoryTableBody = $("#categoryTableBody");
  var $ddlCategory = $("#ddlCategory");
  var $ddlCategoryTemplate = $("#ddlCategoryTemplate");
  var $ddlCategoryInContent = $("#ddlCategoryInContent");

  //type
  var $typeName = $("#typeName");
  var $typeDescription = $("#typeDescription");
  var $typesTableTemplate = $("#typesTableTemplete");
  var $typesTableBody = $("#typesTableBody");
  var $typesDdlTemplate = $("#typesDdlTemplate");
  var $ddlTypes = $("#ddlTypes");
  //remove and edit

  // Content
  var $newContentForm = $("#newContentForm");

  var $title = $("#title");
  var $content = $("#content");
  var $contentTableBody = $("#contentTableBody");
  var $contentTableTemplate = $("#contentTableTemplate");

  $(document).on("click", ".btnRemoveContent", function() {
    var tr = $(this).closest("tr");
    console.log(tr.data("id"));
    axios.put("/api/content/remove/" + tr.data("id")).then(content => {
      populateContentTable();
    });
  });

  // get all content
  function populateContentTable() {
    getAllContent().then(contents => {
      var newContent = addIndexTo(decodeContentField(contents));
      var template = Mustache.render($contentTableTemplate.html(), {
        contents: newContent
      });
      $contentTableBody.html(template);
      console.log(newContent);
    });
  }

  populateContentTable();
  //submit content

  $newContentForm.on("submit", function(e) {
    e.preventDefault();

    var contentData = htmlEncode(CKEDITOR.instances.content.getData());

    var contentFormData = new FormData();
    contentFormData.set("categoryId", $ddlCategoryInContent.val());
    contentFormData.set("typeId", $ddlTypes.val());
    contentFormData.set("title", $title.val());
    contentFormData.set("file", $("input[type=file]")[0].files[0]);
    contentFormData.set("content", contentData);

    axios({
      method: "post",
      url: "/api/content/save",
      data: contentFormData
    })
      .then(res => {
        populateContentTable();

        console.log("successfull");
      })
      .catch(err => {
        console.log(err);
      });
  });

  $ddlCategory.on("change", function() {
    populateCategoryTypes($(this).val());
  });
  $ddlCategoryInContent.on("change", function() {
    populateCategoryTypes($(this).val());
  });

  // Populate type function
  function populateCategoryTypes(catId) {
    getTypesByCategory(catId)
      .then(types => {
        let newCatTypes;
        if (Object.keys(types).length != 0) {
          newCatTypes = addIndexTo(types);
          console.log(newCatTypes);
          var typeTemplate = Mustache.render($typesTableTemplate.html(), {
            catTypes: newCatTypes
          });
          $typesTableBody.html(typeTemplate);
        } else {
          $typesTableBody.html(
            "there are no types under this category, please add new types"
          );
        }
        var ddlTemplate = Mustache.render($typesDdlTemplate.html(), {
          catTypes: newCatTypes
        });
        $ddlTypes.html(ddlTemplate);
      })
      .catch(err => {
        console.log(err);
      });
  }

  // Populate category function
  function populateCategories() {
    // get categories
    getCategories().then(categories => {
      let newCategories;
      if (categories) {
        newCategories = addIndexTo(categories);
      }

      var template = Mustache.render($categoryTableTemplate.html(), {
        categories: newCategories
      });
      $categoryTableBody.html(template);
      var ddlTemplate = Mustache.render($ddlCategoryTemplate.html(), {
        categories: newCategories
      });
      $ddlCategory.html(ddlTemplate);
      $ddlCategoryInContent.html(ddlTemplate);
    });
  }
  populateCategories();

  // post category
  $newCategoryForm.submit(function(e) {
    e.preventDefault();

    const filteredName = $catName.val().replace(/\s/gi, "_");
    console.log(filteredName);
    // category data
    var categoryData = new FormData();
    categoryData.set("name", filteredName);
    categoryData.set("description", $catDescription.val());
    axios({
      method: "post",
      url: "/api/category/save",
      data: categoryData
    }).then(function(res) {
      populateCategories();
    });
  });
  populateTypes();
  //get types
  function populateTypes() {
    //get all types
    getTypes().then(types => {
      // TODO populate types table
      // console.log(types);
    });
  }
  //post types
  $newTypeForm.submit(function(e) {
    e.preventDefault();
    var typeFormData = new FormData();
    // const filteredName = $typeName.val().replace(/\s/gi, "_");
    // console.log(filteredName);
    typeFormData.set("categoryId", $ddlCategory.val());

    typeFormData.set("name", $typeName.val().replace(/\s/gi, "_"));
    typeFormData.set("description", $typeDescription.val());

    axios({
      method: "post",
      url: "/api/type/save",
      data: typeFormData
    }).then(res => {
      console.log(res);
      populateCategoryTypes($ddlCategory.val());
    });
    console.log(typeFormData.get("name"));
  });
});
