/* eslint-disable */
$(document).ready(function() {
  var $renovationContainer = $("#renovationContainer");
  var $renovationTemplate = $("#renovationTemplate");

  var $constructionTemplate = $("#constructionTemplate");
  var $constructionContainer = $("#constructionContainer");

  var $conImageTemplate = $("#conImageTemplate");
  var $conImageContainer = $("#conImageContainer");

  getContentByType("renovation").then(renovationData => {
    var template = Mustache.render($renovationTemplate.html(), {
      renovation: decodeContentField(renovationData)
    });
    $renovationContainer.html(template);
  });

  getContentByType("construction").then(constructionData => {
    var template = Mustache.render($constructionTemplate.html(), {
      construction: decodeContentField(constructionData)
    });
    $constructionContainer.html(template);
  });
  getContentByType("construction_images").then(imageData => {
    var template = Mustache.render($conImageTemplate.html(), {
      images: imageData
    });
    $conImageContainer.html(template);
  });
});
