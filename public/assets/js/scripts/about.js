/* eslint-disable */
$(document).ready(function() {
  var $aboutContainer = $("#aboutContainer");
  var $aboutTemplate = $("#aboutTemplate");

  getContentByType("about_us").then(aboutData => {
    var template = Mustache.render($aboutTemplate.html(), {
      about: decodeContentField(aboutData)
    });
    $aboutContainer.html(template);
    console.log(aboutData);
  });
});
