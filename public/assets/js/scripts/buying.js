/* eslint-disable */
$(document).ready(function() {
  var $stepsFirstContainer = $("#stepsFirstContainer");
  var $stepsSecondContainer2 = $("#stepsSecondContainer");
  var $stepsTemplate = $("#stepsTemplate");
  var $stepsSecondTemplate = $("#stepsSecontTemplate");
  var $otherFacilityContainer = $("#otherFacilityContainer");
  var $otherFacilityTemplate = $("#otherFacilityTemplate");

  getContentByType("other_facilities").then(othersData => {
    var template = Mustache.render($otherFacilityTemplate.html(), {
      others: decodeContentField(othersData)
    });
    $otherFacilityContainer.html(template);
  });
  getContentByType("buying_process").then(stepsData => {
    var template1 = Mustache.render($stepsTemplate.html(), {
      steps: decodeContentField(stepsData).slice(-3)
    });
    var template2 = Mustache.render($stepsSecondTemplate.html(), {
      steps2: decodeContentField(stepsData).slice(3)
    });
    $stepsFirstContainer.html(template1);
    $stepsSecondContainer2.html(template2);
  });

  console.log("working");
});
