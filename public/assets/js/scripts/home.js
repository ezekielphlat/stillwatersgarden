/* eslint-disable */
$(document).ready(function() {
  var $sliderList = $("#sliderList");
  var $sliderTemplate = $("#sliderTemplate");
  var $facilitiesCardContainer = $("#facilitiesCardContainer");
  var $facilitiesTemplate = $("#facilitiesTemplate");
  var $reviewContainer = $("#reviewContainer");
  var $reviewTemplate = $("#reviewTemplate");

  getContentByType("review").then(reviewDate => {
    var template = Mustache.render($reviewTemplate.html(), {
      reviews: decodeContentField(reviewDate)
    });
    $reviewContainer.html(template);
  });
  getContentByType("facilities_cards").then(facilitiesData => {
    var template = Mustache.render($facilitiesTemplate.html(), {
      facilities: decodeContentField(facilitiesData)
    });
    $facilitiesCardContainer.html(template);
  });

  getContentByType("slider").then(slidesData => {
    var template = Mustache.render($sliderTemplate.html(), {
      slides: slidesData
    });
    // if (Object.keys(slidesData).length !== 0) {
    //   $sliderList.html(template);
    // }
    $sliderList.html(template);
    $(".flexslider").flexslider({
      animation: "slide",
      start: function(slider) {
        $("body").removeClass("loading");
      }
    });

    $(".faded-slides").flexslider({
      animation: "slide",
      slideshow: true
    });

    console.log(slidesData);
  });

  console.log("working");
});
