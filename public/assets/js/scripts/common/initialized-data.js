/* eslint-disable */

function getRoles() {
  return axios.get("/api/role").then(res => res.data.roles);
}

function getGenders() {
  return axios.get("/api/gender").then(res => res.data.genders);
}
function getUsers() {
  return axios.get("/api/user").then(res => res.data.users);
}
function getCategories() {
  return axios.get("/api/category").then(res => res.data.cats);
}
function getTypes() {
  return axios.get("/api/type").then(res => res.data.types);
}
function getTypesByCategory(catId) {
  return axios.get("/api/type/" + catId).then(res => res.data.types);
}
function addIndexTo(data) {
  return data.map(d => (d = { index: data.indexOf(d) + 1, ...d }));
}

function decodeContentField(data) {
  return data.map(d => (d = { decodeContent: htmlDecode(d.content), ...d }));
}
function activateAccount(id) {
  return axios.post("/api/user/activate", { id }).then(res => res.data.user);
}
function suspendAccount(id) {
  return axios.post("/api/user/suspend", { id });
}

function getAllContent() {
  return axios.get("/api/content/").then(res => res.data.content);
}
function getContentByType(type) {
  return axios.get("/api/content/" + type).then(res => res.data.content);
}

// Helper function
//decode value of text area
function htmlDecode(value) {
  return $("<textarea/>")
    .html(value)
    .text();
}

//encode value of text area
function htmlEncode(value) {
  return $("<textarea/>")
    .text(value)
    .html();
}
// function login(credentials) {
//   return axios.post("/api/auth", { credentials }).then(res => res);
// }
// // function register(registraionData) {
// //   return axios
// //     .post("/api/auth", { registraionData })
// //     .then(res => res.data.user);
// // }
