/*eslint-disable */

$(document).ready(function() {
  var $residentLink = $("#liResident");
  var $btnLogin = $("#btnLogin");
  var $btnLogout = $(".btnLogout");
  let decoded;

  if (!!localStorage.stillwatersJWT) {
    $btnLogin.addClass("d-none");
    $btnLogout.removeAttr("hidden");
    $residentLink.removeAttr("hidden");
    decoded = jwt_decode(window.localStorage.stillwatersJWT);
    // console.log(decoded);
  }
  $btnLogout.on("click", function() {
    localStorage.removeItem("stillwatersJWT");
    window.location.href = "/";
  });
});
