/* eslint-disable */
$(document).ready(function() {
  var $hiddenToken = $("#hiddenToken");
  var $loadingProcess = $("#loadingProcess");
  var $invalidProcess = $("#invalidProcess");
  var $successProcess = $("#sucessProcess");

  console.log($hiddenToken.val());

  axios({
    method: "post",
    url: "/api/auth/confirmation/" + $hiddenToken.val()
  })
    .then(function(userData) {
      const { user } = userData.data;

      $successProcess.removeClass("d-none");

      localStorage.stillwatersJWT = user.token;
      if (user.role !== "admin") {
        window.location.href = "/";
      } else {
        window.location.href = "/dashboard";
      }
    })
    .catch(err => {
      $invalidProcess.removeClass("d-none");
    });
  console.log("working");
});
