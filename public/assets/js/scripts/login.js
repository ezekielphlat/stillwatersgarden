/* eslint-disable */
$(document).ready(function() {
  var $email = $("#email");
  var $password = $("#password");
  var $error = $("#errorGlobal");
  // function loginData() {
  //   return {
  //     email: $email.val(),
  //     password: $password.val()
  //   };
  // }
  $("form input").on("focus", function() {
    $error.fadeOut(3000);
    $error.html();
  });
  $("form#loginForm").submit(function(e) {
    e.preventDefault();

    var loginData = new FormData();
    loginData.set("email", $email.val());
    loginData.set("password", $password.val());

    axios({
      method: "post",
      url: "/api/auth",
      data: loginData
    })
      .then(function(userData) {
        const { user } = userData.data;
        localStorage.stillwatersJWT = user.token;
        if (user.role !== "admin") {
          window.location.href = "/";
        } else {
          window.location.href = "/dashboard";
        }
        console.log(user);
      })
      .catch(function(err) {
        if (err) {
          $error.fadeIn(3000);

          $error.removeClass("d-none");
          $error.html("Username or password incorrect");
        }
      });
  });
});
